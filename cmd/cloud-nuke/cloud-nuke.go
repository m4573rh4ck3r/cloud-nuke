// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"
	"time"

	serviceusage "cloud.google.com/go/serviceusage/apiv1"
	serviceusagepb "cloud.google.com/go/serviceusage/apiv1/serviceusagepb"
	"github.com/spf13/cobra"
	"google.golang.org/api/option"

	"gitlab.com/m4573rh4ck3r/cloud-nuke/pkg/gcp"
)

var (
	dryRun           bool
	logLevel         int
	gcpLocations     string
	gcpProject       string
	skipApiPatterns  []string
	force            bool
	supportedGcpApis = []string{
		"artifactregistry",
		"bigquery",
		"compute",
		"container",
		"dataproc",
		"cloudkms",
		"redis",
		"run",
		"secretmanager",
		"spanner",
		"storage-component",
		"tpu",
	}
	gcpZones   []string
	gcpRegions []string
	gcpApis    []string
)

func init() {
	rootCmd.AddCommand(
		gcpCmd,
	)

	rootCmd.PersistentFlags().IntVarP(&logLevel, "verbose", "v", -4, "log level (debug: -4, info: 0, warn: 4, error only: 8)")
	rootCmd.PersistentFlags().BoolVar(&dryRun, "dry-run", true, "only print what would be deleted")

	gcpCmd.Flags().StringVar(&gcpLocations, "locations", "europe-west1", "restrict the scope to certain locations matching the regex")
	gcpCmd.Flags().StringVar(&gcpProject, "project", "", "name of the gcp project")
	gcpCmd.Flags().StringSliceVar(&skipApiPatterns, "skip-apis", []string{"^storage$", "^iam.*$", "^sql.*$"}, "skip apis matching the provided regex")
	gcpCmd.Flags().BoolVar(&force, "force", false, "force deletion of all resources")
}

// -------------------------------------------------- root cmd --------------------------------------------------
var rootCmd = &cobra.Command{
	Use:   "cloud-nuke",
	Short: "nuke cloud resources",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		l := slog.Level(logLevel)
		logHandler := slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level:     l,
			AddSource: true,
			ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == slog.TimeKey {
					a.Value = slog.StringValue(time.Now().Format("15:04:05.00"))
				}
				if a.Key == slog.SourceKey {
					a.Value = slog.StringValue(strings.ReplaceAll(strings.Trim(filepath.Base(a.Value.String()), "}"), " ", " line:"))
				}

				return a
			},
		})

		slog.SetDefault(slog.New(logHandler))
	},
}

// -------------------------------------------------- gcp cmd --------------------------------------------------
var gcpCmd = &cobra.Command{
	Use: "gcp",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		for _, supportedGcpApi := range supportedGcpApis {
			matched := false
			for _, pattern := range skipApiPatterns {
				if regexp.MustCompile(pattern).MatchString(supportedGcpApi) {
					matched = true
					break
				}
			}
			if !matched {
				gcpApis = append(gcpApis, supportedGcpApi)
			} else {
				slog.InfoContext(ctx, fmt.Sprintf("skipping projects/%s/services/%s.googleapis.com", gcpProject, supportedGcpApi))
			}
		}

		opts, err := gcp.FindCredentials(ctx)
		if err != nil {
			slog.ErrorContext(ctx, err.Error())
			os.Exit(1)
		}

		clientStore, err := gcp.InitClients(ctx, opts, gcpProject, gcpApis)
		if err != nil {
			slog.ErrorContext(ctx, err.Error())
			os.Exit(1)
		}

		regions, err := clientStore.GetComputeRegions(ctx, gcpProject)
		if err != nil {
			slog.ErrorContext(ctx, err.Error())
			os.Exit(1)
		}

		for _, region := range regions {
			if regexp.MustCompile(gcpLocations).MatchString(region.GetName()) {
				gcpRegions = append(gcpRegions, region.GetName())
			}
		}

		zones, err := clientStore.GetComputeZones(ctx, gcpProject)
		if err != nil {
			slog.ErrorContext(ctx, err.Error())
			os.Exit(1)
		}

		for _, zones := range zones {
			if regexp.MustCompile(gcpLocations).MatchString(zones.GetName()) {
				gcpZones = append(gcpZones, zones.GetName())
			}
		}

		// -------------------------------------------------- artifactregistry api --------------------------------------------------
		if slices.Contains(gcpApis, "artifactregistry") {
			artifactregistryApiEnabled, err := gcpApiEnabled(ctx, opts, "artifactregistry")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if artifactregistryApiEnabled {
				slog.DebugContext(ctx, "artifactregistry api is enabled")

				artifactregistryRepository, err := clientStore.GetArtifactregistryRepositories(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(artifactregistryRepository) == 0 {
					slog.InfoContext(ctx, "no gcp artifactregistry repositories to delete")
				} else {
					if err := clientStore.DeleteArtifactregistryRepositories(ctx, artifactregistryRepository, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "artifactregistry api is disabled")
			}
		}

		// -------------------------------------------------- bigquery api --------------------------------------------------
		if slices.Contains(gcpApis, "bigquery") {
			bigqueryApiEnabled, err := gcpApiEnabled(ctx, opts, "bigquery")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if bigqueryApiEnabled {
				slog.DebugContext(ctx, "bigquery api is enabled")

				bigqueryDataset, err := clientStore.GetBigqueryDatasets(ctx, gcpProject, gcpZones)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(bigqueryDataset) == 0 {
					slog.InfoContext(ctx, "no gcp bigquery datasets to delete")
				} else {
					if err := clientStore.DeleteBigqueryDatasets(ctx, bigqueryDataset, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "bigquery api is disabled")
			}
		}

		// -------------------------------------------------- compute api --------------------------------------------------
		if slices.Contains(gcpApis, "compute") {
			computeApiEnabled, err := gcpApiEnabled(ctx, opts, "compute")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if computeApiEnabled {
				slog.DebugContext(ctx, "compute api is enabled")

				computeAddress, err := clientStore.GetComputeAddresses(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(computeAddress) == 0 {
					slog.InfoContext(ctx, "no gcp compute addresses to delete")
				} else {
					if err := clientStore.DeleteComputeAddresses(ctx, computeAddress, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

				computeBackendService, err := clientStore.GetComputeBackendServices(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(computeBackendService) == 0 {
					slog.InfoContext(ctx, "no gcp compute backendServices to delete")
				} else {
					if err := clientStore.DeleteComputeBackendServices(ctx, computeBackendService, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

				computeDisk, err := clientStore.GetComputeDisks(ctx, gcpProject, gcpZones)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(computeDisk) == 0 {
					slog.InfoContext(ctx, "no gcp compute disks to delete")
				} else {
					if err := clientStore.DeleteComputeDisks(ctx, computeDisk, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

				computeNetwork, err := clientStore.GetComputeNetworks(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(computeNetwork) == 0 {
					slog.InfoContext(ctx, "no gcp compute networks to delete")
				} else {
					if err := clientStore.DeleteComputeNetworks(ctx, computeNetwork, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "compute api is disabled")
			}
		}

		// -------------------------------------------------- container api --------------------------------------------------
		if slices.Contains(gcpApis, "container") {
			containerApiEnabled, err := gcpApiEnabled(ctx, opts, "container")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if containerApiEnabled {
				slog.DebugContext(ctx, "container api is enabled")

				containerCluster, err := clientStore.GetContainerClusters(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(containerCluster) == 0 {
					slog.InfoContext(ctx, "no gcp container clusters to delete")
				} else {
					if err := clientStore.DeleteContainerClusters(ctx, containerCluster, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "container api is disabled")
			}
		}

		// -------------------------------------------------- dataproc api --------------------------------------------------
		if slices.Contains(gcpApis, "dataproc") {
			dataprocApiEnabled, err := gcpApiEnabled(ctx, opts, "dataproc")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if dataprocApiEnabled {
				slog.DebugContext(ctx, "dataproc api is enabled")

				dataprocCluster, err := clientStore.GetDataprocClusters(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(dataprocCluster) == 0 {
					slog.InfoContext(ctx, "no gcp dataproc clusters to delete")
				} else {
					if err := clientStore.DeleteDataprocClusters(ctx, dataprocCluster, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "dataproc api is disabled")
			}
		}

		// -------------------------------------------------- kms api --------------------------------------------------
		if slices.Contains(gcpApis, "cloudkms") {
			kmsApiEnabled, err := gcpApiEnabled(ctx, opts, "cloudkms")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if kmsApiEnabled {
				slog.DebugContext(ctx, "kms api is enabled")

				kmsKeyRing, err := clientStore.GetKmsKeyRings(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(kmsKeyRing) == 0 {
					slog.InfoContext(ctx, "no gcp kms keyRings to delete")
				} else {
					if err := clientStore.DeleteKmsKeyRings(ctx, kmsKeyRing, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "kms api is disabled")
			}
		}

		// -------------------------------------------------- redis api --------------------------------------------------
		if slices.Contains(gcpApis, "redis") {
			redisApiEnabled, err := gcpApiEnabled(ctx, opts, "redis")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if redisApiEnabled {
				slog.DebugContext(ctx, "redis api is enabled")

				redisInstance, err := clientStore.GetRedisInstances(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(redisInstance) == 0 {
					slog.InfoContext(ctx, "no gcp redis instances to delete")
				} else {
					if err := clientStore.DeleteRedisInstances(ctx, redisInstance, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "redis api is disabled")
			}
		}

		// -------------------------------------------------- run api --------------------------------------------------
		if slices.Contains(gcpApis, "run") {
			runApiEnabled, err := gcpApiEnabled(ctx, opts, "run")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if runApiEnabled {
				slog.DebugContext(ctx, "run api is enabled")

				runService, err := clientStore.GetRunServices(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(runService) == 0 {
					slog.InfoContext(ctx, "no gcp run instances to delete")
				} else {
					if err := clientStore.DeleteRunServices(ctx, runService, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "run api is disabled")
			}
		}

		// -------------------------------------------------- secretmanager api --------------------------------------------------
		if slices.Contains(gcpApis, "secretmanager") {
			secretmanagerApiEnabled, err := gcpApiEnabled(ctx, opts, "secretmanager")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if secretmanagerApiEnabled {
				slog.DebugContext(ctx, "secretmanager api is enabled")

				secretmanagerSecret, err := clientStore.GetSecretmanagerSecrets(ctx, gcpProject, gcpZones)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(secretmanagerSecret) == 0 {
					slog.InfoContext(ctx, "no gcp secretmanager secrets to delete")
				} else {
					if err := clientStore.DeleteSecretmanagerSecrets(ctx, secretmanagerSecret, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "secretmanager api is disabled")
			}
		}

		// -------------------------------------------------- spanner api --------------------------------------------------
		if slices.Contains(gcpApis, "spanner") {
			spannerApiEnabled, err := gcpApiEnabled(ctx, opts, "spanner")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if spannerApiEnabled {
				slog.DebugContext(ctx, "spanner api is enabled")

				spannerInstance, err := clientStore.GetSpannerInstances(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(spannerInstance) == 0 {
					slog.InfoContext(ctx, "no gcp spanner instances to delete")
				} else {
					if err := clientStore.DeleteSpannerInstances(ctx, spannerInstance, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "spanner api is disabled")
			}
		}

		// -------------------------------------------------- storage api --------------------------------------------------
		if slices.Contains(gcpApis, "storage-component") {
			storageApiEnabled, err := gcpApiEnabled(ctx, opts, "storage-component")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if storageApiEnabled {
				slog.DebugContext(ctx, "storage api is enabled")

				storageBucket, err := clientStore.GetStorageBuckets(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(storageBucket) == 0 {
					slog.InfoContext(ctx, "no gcp storage buckets to delete")
				} else {
					if err := clientStore.DeleteStorageBuckets(ctx, storageBucket, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "storage api is disabled")
			}
		}

		// -------------------------------------------------- tpu api --------------------------------------------------
		if slices.Contains(gcpApis, "tpu") {
			tpuApiEnabled, err := gcpApiEnabled(ctx, opts, "tpu")
			if err != nil {
				slog.ErrorContext(ctx, err.Error())
				os.Exit(1)
			}

			if tpuApiEnabled {
				slog.DebugContext(ctx, "tpu api is enabled")

				tpuNode, err := clientStore.GetTpuNodes(ctx, gcpProject, gcpRegions)
				if err != nil {
					slog.ErrorContext(ctx, err.Error())
					os.Exit(1)
				}

				if len(tpuNode) == 0 {
					slog.InfoContext(ctx, "no gcp tpu nodes to delete")
				} else {
					if err := clientStore.DeleteTpuNodes(ctx, tpuNode, dryRun); err != nil {
						slog.ErrorContext(ctx, err.Error())
						os.Exit(1)
					}
				}

			} else {
				slog.DebugContext(ctx, "tpu api is disabled")
			}
		}

	},
}

// gcpApiEnabled returns a boolean indicating whether the provided gcp api is enabled
func gcpApiEnabled(ctx context.Context, opts *option.ClientOption, apiName string) (bool, error) {
	client, err := serviceusage.NewClient(ctx, *opts)
	if err != nil {
		return false, err
	}

	resp, err := client.GetService(ctx, &serviceusagepb.GetServiceRequest{Name: fmt.Sprintf("projects/%s/services/%s.googleapis.com", gcpProject, apiName)})
	if err != nil {
		return false, err
	}

	if resp.GetState().String() == "ENABLED" {
		return true, nil
	}
	if resp.GetState().String() == "DISABLED" {
		return false, nil
	}

	return false, fmt.Errorf("%s api state is %s", apiName, resp.GetState().String())
}

// -------------------------------------------------- main --------------------------------------------------
func main() {
	rootCmd.Execute()
}
