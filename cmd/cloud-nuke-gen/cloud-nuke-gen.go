// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:generate go run cloud-nuke-gen.go
package main

import (
	"os"
	"text/template"

	_ "embed"
)

type Flag struct {
	Name           string
	Type           string
	Default        string
	Description    string
	FlagType       string
	FlagTypeTitled string
	VarName        string
}

type Cmd struct {
	Name       string
	NameTitled string
	Apis       []Api
	Flags      []Flag
}

type Resource struct {
	Name             string
	LocationType     string
	Plural           string
	NameTitled       string
	PluralNameTitled string
}

type Api struct {
	Name       string
	NameTitled string
	Resources  []Resource
	Endpoint   string
}

var (
	apis = []Api{
		{
			Name:       "artifactregistry",
			NameTitled: "Artifactregistry",
			Endpoint:   "artifactregistry",
			Resources: []Resource{
				{
					Name:             "repository",
					LocationType:     "region",
					Plural:           "repositories",
					NameTitled:       "Repository",
					PluralNameTitled: "Repositories",
				},
			},
		},
		{
			Name:       "bigquery",
			NameTitled: "Bigquery",
			Endpoint:   "bigquery",
			Resources: []Resource{
				{
					Name:             "dataset",
					LocationType:     "",
					Plural:           "datasets",
					NameTitled:       "Dataset",
					PluralNameTitled: "Datasets",
				},
			},
		},
		{
			Name:       "compute",
			NameTitled: "Compute",
			Endpoint:   "compute",
			Resources: []Resource{
				{
					Name:             "address",
					LocationType:     "region",
					Plural:           "addresses",
					NameTitled:       "Address",
					PluralNameTitled: "Addresses",
				},
				{
					Name:             "backendService",
					LocationType:     "region",
					Plural:           "backendServices",
					NameTitled:       "BackendService",
					PluralNameTitled: "BackendServices",
				},
				{
					Name:             "disk",
					LocationType:     "zone",
					Plural:           "disks",
					NameTitled:       "Disk",
					PluralNameTitled: "Disks",
				},
				{
					Name:             "network",
					LocationType:     "region",
					Plural:           "networks",
					NameTitled:       "Network",
					PluralNameTitled: "Networks",
				},
			},
		},
		{
			Name:       "container",
			NameTitled: "Container",
			Endpoint:   "container",
			Resources: []Resource{
				{
					Name:             "cluster",
					LocationType:     "region",
					Plural:           "clusters",
					NameTitled:       "Cluster",
					PluralNameTitled: "Clusters",
				},
			},
		},
		{
			Name:       "dataproc",
			NameTitled: "Dataproc",
			Endpoint:   "dataproc",
			Resources: []Resource{
				{
					Name:             "cluster",
					LocationType:     "region",
					NameTitled:       "Cluster",
					Plural:           "clusters",
					PluralNameTitled: "Clusters",
				},
			},
		},
		{
			Name:       "kms",
			NameTitled: "Kms",
			Endpoint:   "cloudkms",
			Resources: []Resource{
				{
					Name:             "keyRing",
					LocationType:     "region",
					NameTitled:       "KeyRing",
					Plural:           "keyRings",
					PluralNameTitled: "KeyRings",
				},
			},
		},
		{
			Name:       "redis",
			NameTitled: "Redis",
			Endpoint:   "redis",
			Resources: []Resource{
				{
					Name:             "instance",
					LocationType:     "region",
					Plural:           "instances",
					NameTitled:       "Instance",
					PluralNameTitled: "Instances",
				},
			},
		},
		{
			Name:       "run",
			NameTitled: "Run",
			Endpoint:   "run",
			Resources: []Resource{
				{
					Name:             "service",
					LocationType:     "region",
					Plural:           "instances",
					NameTitled:       "Service",
					PluralNameTitled: "Services",
				},
			},
		},
		{
			Name:       "secretmanager",
			NameTitled: "Secretmanager",
			Endpoint:   "secretmanager",
			Resources: []Resource{
				{
					Name:             "secret",
					LocationType:     "",
					NameTitled:       "Secret",
					Plural:           "secrets",
					PluralNameTitled: "Secrets",
				},
			},
		},
		{
			Name:       "spanner",
			NameTitled: "Spanner",
			Endpoint:   "spanner",
			Resources: []Resource{
				{
					Name:             "instance",
					LocationType:     "region",
					Plural:           "instances",
					NameTitled:       "Instance",
					PluralNameTitled: "Instances",
				},
			},
		},
		{
			Name:       "storage",
			NameTitled: "Storage",
			Endpoint:   "storage-component",
			Resources: []Resource{
				{
					Name:             "bucket",
					LocationType:     "region",
					Plural:           "buckets",
					NameTitled:       "Bucket",
					PluralNameTitled: "Buckets",
				},
			},
		},
		{
			Name:       "tpu",
			NameTitled: "Tpu",
			Endpoint:   "tpu",
			Resources: []Resource{
				{
					Name:             "node",
					LocationType:     "region",
					Plural:           "nodes",
					NameTitled:       "Node",
					PluralNameTitled: "Nodes",
				},
			},
		},
	}

	cmds = []Cmd{
		{
			Name:       "gcp",
			NameTitled: "Gcp",
			Apis:       apis,
			Flags: []Flag{
				{
					Name:           "locations",
					VarName:        "gcpLocations",
					Type:           "string",
					FlagType:       "string",
					FlagTypeTitled: "String",
					Default:        "\"europe-west1\"",
					Description:    "restrict the scope to certain locations matching the regex",
				},
				{
					Name:           "project",
					VarName:        "gcpProject",
					Type:           "string",
					FlagType:       "string",
					FlagTypeTitled: "String",
					Default:        "\"\"",
					Description:    "name of the gcp project",
				},
				{
					Name:           "skip-apis",
					VarName:        "skipApiPatterns",
					Type:           "[]string",
					FlagType:       "stringSlice",
					FlagTypeTitled: "StringSlice",
					Default:        "[]string{\"^storage$\", \"^iam.*$\", \"^sql.*$\"}",
					Description:    "skip apis matching the provided regex",
				},
				{
					Name:           "force",
					VarName:        "force",
					Type:           "bool",
					FlagType:       "bool",
					FlagTypeTitled: "Bool",
					Default:        "false",
					Description:    "force deletion of all resources",
				},
			},
		},
	}

	//go:embed cmd.tmpl
	cmdTmpl string

	funcMap = template.FuncMap{}
)

func main() {
	tmpl := template.Must(template.New("cmd").Funcs(funcMap).Parse(cmdTmpl))
	f, err := os.OpenFile("../cloud-nuke/cloud-nuke.go", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if err := tmpl.Execute(f, cmds); err != nil {
		panic(err)
	}
}
