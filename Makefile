# Copyright 2024 m4573RH4CK3R
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# set target and source variables

bin_PROGRAMS ?= cloud-nuke

cloud-nuke_SOURCES ?= cmd/cloud-nuke/cloud-nuke.go
cloud-nuke_DEPENDENCIES ?= libs

target_architectures ?= linux/amd64 linux/arm64 darwin/amd64 darwin/arm64


# set go env variables

CGO_ENABLED ?= 0

GOPATH ?= $(HOME)/go

GOBIN ?= $(GOPATH)/bin

GOOS ?= linux

GOARCH ?= amd64


# set compiler variables and flags

cc ?= go

CCLD ?= $(cc) build

version ?= "$(shell git rev-parse HEAD)-dev"

ifeq ($(ldflags),)
	ldflags = -w -s
	ldflags += -X "gitlab.com/m4573rh4ck3r/cloud-nuke/internal/cmd.version=$(version)"
endif

ifeq ($(installflags),)
	installflags = -s
	installflags += -D
endif

verbose ?= true

ifeq ($(buildflags),)
ifeq ($(verbose),true)
	buildflags = -x
endif
endif

ifeq ($(installflags),)
ifeq ($(verbose),true)
	installflags = -v
endif
endif

ifeq ($(LiNK),)
	LINK = CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) $(CCLD) $(buildflags) -ldflags "$(ldflags)"
endif


# set directory variables

srcdir := $(CURDIR)

ifeq ($(prefix),)
	prefix = /usr
endif

ifeq ($(bindir),)
	bindir := bin
endif

ifeq ($(builddir),)
	builddir = bin
endif

ifeq ($(confdir),)
	confdir := $(prefix)/etc/$(bin_PROGRAMS)
endif


# static targets

all: generate build

addlicense:
	@addlicense -c "M4573RH4CK3R" -l apache cmd pkg

build: $(builddir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), $(LINK) -o "$(builddir)/$(bin_PROGRAM)" "$($(bin_PROGRAM)_SOURCES)";)

clean:
	@rm -rvf "$(builddir)"
	@$(cc) clean -cache

dist: $(builddir)
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), GOOS=$(GOOS) GOARCH=$(GOARCH) $(LINK) -o "$(builddir)/$(bin_PROGRAM)-$(GOOS)-$(GOARCH)" "$($(bin_PROGRAM)_SOURCES)";)

dist-all: $(builddir)
	@$(foreach target_arch,$(target_architectures), GOOS=$(shell echo $(target_arch) | sed 's/\/.*//g' ) GOARCH=$(shell echo $(target_arch) | sed 's/^.*\///g') $(MAKE) dist;)

generate:
	@./hack/generate.bash

install:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), install $(installflags) "$(builddir)/$(bin_PROGRAM)" "$(prefix)/$(bindir)";)

libs:
	@GODEBUG=installgoroot=all go install std

tidy:
	@$(cc) mod tidy

uninstall:
	@$(foreach bin_PROGRAM,$(bin_PROGRAMS), rm -rvf "$(prefix)/$(bindir)/$(bin_PROGRAM)";)

upgrade-deps:
	@$(cc) get -u ./...

test:
	@$(cc) test -v ./...

.PHONY:  addlicense all build clean dist generate install libs test tidy uninstall upgrade-deps


# dynamic targets

$(builddir):
	@mkdir -p $@

$(gendir):
	@mkdir -p $@
