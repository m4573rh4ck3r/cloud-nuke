// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/run/apiv2/runpb"
	"google.golang.org/api/iterator"
)

// GetRunServices returns a list of gcp cloud run serivces in the provided gcp project and locations
func (c *ClientStore) GetRunServices(ctx context.Context, project string, locations []string) ([]*runpb.Service, error) {
	var services []*runpb.Service

	for _, location := range locations {
		req := &runpb.ListServicesRequest{
			Parent: fmt.Sprintf("projects/%s/locations/%s", project, location),
		}

		it := c.RunServicesClient.ListServices(ctx, req)
		for {
			service, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			services = append(services, service)
		}
	}

	return services, nil
}

// DeleteRunServices deletes the provided gcp cloud run services
func (c *ClientStore) DeleteRunServices(ctx context.Context, runServices []*runpb.Service, dryRun bool) error {
	if dryRun {
		for _, service := range runServices {
			slog.InfoContext(ctx, fmt.Sprintf("redis instance %s will be deleted", service.GetName()), slog.Bool("dry-run", dryRun))
		}
	}

	return nil
}
