// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/alloydb/apiv1/alloydbpb"
	"google.golang.org/api/iterator"
)

// GetAlloyDBInstances returns a list of gcp alloydb instances in the provided gcp project and locations
func (c *ClientStore) GetAlloyDBInstances(ctx context.Context, project string, locations []string) ([]*alloydbpb.Instance, error) {
	var instances []*alloydbpb.Instance

	for _, location := range locations {
		req := &alloydbpb.ListInstancesRequest{
			Parent: fmt.Sprintf("projects/%s/locations/%s/clusters", project, location),
		}
		it := c.AlloyDBAdminClient.ListInstances(ctx, req)
		for {
			instance, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			instances = append(instances, instance)
		}
	}

	return instances, nil
}

// DeleteAlloyDBInstances deletes the provided gcp alloydb instances
func (c *ClientStore) DeleteAlloyDBInstances(ctx context.Context, instances []*alloydbpb.Instance, dryRun bool) error {
	if dryRun {
		for _, instance := range instances {
			slog.InfoContext(ctx, fmt.Sprintf("alloydb instance %s will be deleted", instance.GetName()), slog.Bool("dry-run", dryRun))
		}
	}

	return nil
}
