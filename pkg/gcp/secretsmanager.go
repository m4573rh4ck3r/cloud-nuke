// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"google.golang.org/api/iterator"
)

// GetSecretmanagerSecrets returns a list of gcp secretmanager secrets in the provided gcp project
func (c *ClientStore) GetSecretmanagerSecrets(ctx context.Context, project string, locations []string) ([]*secretmanagerpb.Secret, error) {
	var secrets []*secretmanagerpb.Secret

	req := &secretmanagerpb.ListSecretsRequest{
		Parent: fmt.Sprintf("projects/%s", project),
	}
	it := c.SecretmanagerClient.ListSecrets(ctx, req)
	for {
		secret, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		secrets = append(secrets, secret)
	}

	return secrets, nil
}

// DeleteSecretmanagerSecrets deletes the provided gcp secretmanager secrets
func (c *ClientStore) DeleteSecretmanagerSecrets(ctx context.Context, secrets []*secretmanagerpb.Secret, dryRun bool) error {
	if dryRun {
		for _, secret := range secrets {
			slog.InfoContext(ctx, fmt.Sprintf("secretmanager secret %s will be deleted", secret.GetName()), slog.Bool("dry-run", dryRun))
		}
	}

	return nil
}
