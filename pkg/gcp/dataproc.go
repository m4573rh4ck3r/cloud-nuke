// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/dataproc/v2/apiv1/dataprocpb"
	"google.golang.org/api/iterator"
)

// GetDataprocClusters returns a list of gcp dataproc clusters in the provided gcp project and locations
func (c *ClientStore) GetDataprocClusters(ctx context.Context, project string, locations []string) ([]*dataprocpb.Cluster, error) {
	var clusters []*dataprocpb.Cluster

	req := &dataprocpb.ListClustersRequest{
		ProjectId: project,
		Region:    "global",
	}
	it := c.DataprocClustersClient.ListClusters(ctx, req)
	for {
		resp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		clusters = append(clusters, resp)
	}

	return clusters, nil
}

// DeleteDataprocClusters deletes the provided gcp dataproc clusters
func (c *ClientStore) DeleteDataprocClusters(ctx context.Context, dataprocClusters []*dataprocpb.Cluster, dryRun bool) error {
	if dryRun {
		for _, dataprocCluster := range dataprocClusters {
			slog.InfoContext(ctx, fmt.Sprintf("dataproc cluster %s will be deleted", dataprocCluster.GetClusterName()), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}
