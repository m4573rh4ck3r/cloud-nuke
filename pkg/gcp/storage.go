// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	storage "cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

// GetStorageBuckets returns a list of gcp storage buckets in the provided gcp project and locations
func (c *ClientStore) GetStorageBuckets(ctx context.Context, project string, locations []string) ([]*storage.BucketAttrs, error) {
	var buckets []*storage.BucketAttrs

	it := c.StorageBucketsClient.Buckets(ctx, project)
	for {
		bucket, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		buckets = append(buckets, bucket)
	}

	return buckets, nil
}

// DeleteStorageBuckets deletes the provided gcp storage buckets
func (c *ClientStore) DeleteStorageBuckets(ctx context.Context, storageBuckets []*storage.BucketAttrs, dryRun bool) error {
	if dryRun {
		for _, bucket := range storageBuckets {
			slog.InfoContext(ctx, fmt.Sprintf("storage bucket %s will be deleted", bucket.Name), slog.Bool("dry-run", dryRun))
		}

		return nil
	}

	return nil
}
