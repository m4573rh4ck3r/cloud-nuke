// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/kms/apiv1/kmspb"
	"google.golang.org/api/iterator"
	"google.golang.org/genproto/googleapis/cloud/location"
)

func (c *ClientStore) getKmsLocations(ctx context.Context, project string) ([]*location.Location, error) {
	var locations []*location.Location
	req := &location.ListLocationsRequest{
		Name: fmt.Sprintf("projects/%s", project),
	}
	it := c.KmsKeyManagementClient.ListLocations(ctx, req)
	for {
		location, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		locations = append(locations, location)
	}

	return locations, nil
}

// GetKmsKeyRings returns a list of gcp kms keyRings in the provided gcp project
func (c *ClientStore) GetKmsKeyRings(ctx context.Context, project string, locations []string) ([]*kmspb.KeyRing, error) {
	var keyRings []*kmspb.KeyRing

	kmsLocations, err := c.getKmsLocations(ctx, project)
	if err != nil {
		return nil, err
	}

	for _, kmsLocation := range kmsLocations {
		matched := false
		for _, location := range locations {
			if location == kmsLocation.GetLocationId() {
				matched = true
			}
		}
		if !matched {
			continue
		}

		req := &kmspb.ListKeyRingsRequest{
			Parent: kmsLocation.GetName(),
		}
		it := c.KmsKeyManagementClient.ListKeyRings(ctx, req)
		for {
			keyRing, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			keyRings = append(keyRings, keyRing)
		}
	}

	return keyRings, nil
}

// DeleteKmsKeyRings deletes the provided gcp kms keyRings
func (c *ClientStore) DeleteKmsKeyRings(ctx context.Context, keyRings []*kmspb.KeyRing, dryRun bool) error {
	if dryRun {
		for _, keyRing := range keyRings {
			slog.InfoContext(ctx, fmt.Sprintf("kms keyRing %s will be deleted", keyRing.GetName()), slog.Bool("dry-run", dryRun))
		}
	}

	return nil
}
