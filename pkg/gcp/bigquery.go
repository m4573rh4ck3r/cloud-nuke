// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
)

// GetBigqueryDatasets returns a list of gcp bigquery datasets in the provided gcp project
func (c *ClientStore) GetBigqueryDatasets(ctx context.Context, project string, locations []string) ([]*bigquery.Dataset, error) {
	var datasets []*bigquery.Dataset

	it := c.BigqueryClient.Datasets(ctx)
	for {
		dataset, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		datasets = append(datasets, dataset)
	}

	return datasets, nil
}

// DeleteBigqueryDatasets deletes the provided gcp bigquery datasets
func (c *ClientStore) DeleteBigqueryDatasets(ctx context.Context, datasets []*bigquery.Dataset, dryRun bool) error {
	if dryRun {
		for _, dataset := range datasets {
			slog.InfoContext(ctx, fmt.Sprintf("bigquery dataset %s will be deleted", dataset.DatasetID), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}
