// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/tpu/apiv1/tpupb"
	"google.golang.org/api/iterator"
	"google.golang.org/genproto/googleapis/cloud/location"
)

// GetTpuNodes returns a list of gcp tpu nodes in the provided gcp project and locations
func (c *ClientStore) GetTpuNodes(ctx context.Context, project string, locations []string) ([]*tpupb.Node, error) {
	var nodes []*tpupb.Node
	var tpuLocations []*location.Location

	listLocationsReq := &location.ListLocationsRequest{}
	locationsIterator := c.TpuClient.ListLocations(ctx, listLocationsReq)
	for {
		tpuLocation, err := locationsIterator.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		tpuLocations = append(tpuLocations, tpuLocation)
	}

	for _, tpuLocation := range tpuLocations {
		req := &tpupb.ListNodesRequest{
			Parent: tpuLocation.GetName(),
		}

		it := c.TpuClient.ListNodes(ctx, req)

		for {
			node, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			nodes = append(nodes, node)
		}
	}

	return nodes, nil
}

// DeleteTpuNodes deletes the provided gcp tpu nodes
func (c *ClientStore) DeleteTpuNodes(ctx context.Context, nodes []*tpupb.Node, dryRun bool) error {
	if dryRun {
		for _, node := range nodes {
			slog.InfoContext(ctx, fmt.Sprintf("tpu node %s will be deleted", node.GetName()), slog.Bool("dry-run", dryRun))
		}
	}

	return nil
}
