// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"os"
	"testing"
)

var (
	gcpProject = os.Getenv("CLOUD_NUKE_GCP_PROJECT_ID")
)

func TestInitClients(t *testing.T) {
	ctx := context.Background()
	opts, err := FindCredentials(ctx)
	if err != nil {
		t.Fatal(err)
	}
	_, err = InitClients(ctx, opts, gcpProject, []string{".*"})
}
