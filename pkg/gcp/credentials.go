// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"
)

// findGcpCredentials returns the default gcp credentials with the CloudPlatformScope
func FindCredentials(ctx context.Context) (*option.ClientOption, error) {
	token := os.Getenv("GOOGLE_OAUTH_ACCESS_TOKEN")
	if token != "" {
		tokenSource := oauth2.StaticTokenSource(&oauth2.Token{
			AccessToken: token,
		})
		opts := option.WithTokenSource(tokenSource)
		return &opts, nil
	}

	jsonFile := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	if jsonFile != "" {
		opts := option.WithCredentialsFile(jsonFile)
		return &opts, nil
	}

	creds, err := google.FindDefaultCredentials(ctx, compute.CloudPlatformScope)
	if err != nil {
		return nil, err
	}

	opts := option.WithCredentials(creds)
	return &opts, nil
}
