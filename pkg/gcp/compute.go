// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/compute/apiv1/computepb"
	"google.golang.org/api/iterator"
)

// GetComputeAddresses returns a list of gcp compute addresses in the provided gcp project and locations
func (c *ClientStore) GetComputeAddresses(ctx context.Context, project string, locations []string) ([]*computepb.Address, error) {
	var addresses []*computepb.Address

	for _, location := range locations {

		req := &computepb.ListAddressesRequest{
			Project: project,
			Region:  location,
		}
		it := c.ComputeAddressesClient.List(ctx, req)
		for {
			address, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			addresses = append(addresses, address)
		}
	}

	return addresses, nil
}

// DeleteComputeAddresses deletes the provided gcp compute addresses
func (c *ClientStore) DeleteComputeAddresses(ctx context.Context, addresses []*computepb.Address, dryRun bool) error {
	if dryRun {
		for _, address := range addresses {
			slog.InfoContext(ctx, fmt.Sprintf("gcp compute address %s will be deleted", address.GetSelfLink()), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}

// GetComputeBackendServices returns a list of gcp compute backend services in the provided gcp project and locations
func (c *ClientStore) GetComputeBackendServices(ctx context.Context, project string, locations []string) ([]*computepb.BackendService, error) {
	var backendServices []*computepb.BackendService

	req := &computepb.ListBackendServicesRequest{
		Project: project,
	}
	it := c.ComputeBackendServicesClient.List(ctx, req)
	for {
		backendService, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		backendServices = append(backendServices, backendService)
	}

	return backendServices, nil
}

// DeleteComputeBackendServices deletes the provided gcp compute backend services
func (c *ClientStore) DeleteComputeBackendServices(ctx context.Context, backendServices []*computepb.BackendService, dryRun bool) error {
	if dryRun {
		for _, backendService := range backendServices {
			slog.InfoContext(ctx, fmt.Sprintf("gcp compute backend service %s will be deleted", backendService.GetSelfLink()), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}

// GetComputeDisks returns a list of gcp compute disks in the provided gcp project and locations
func (c *ClientStore) GetComputeDisks(ctx context.Context, project string, zones []string) ([]*computepb.Disk, error) {
	var disks []*computepb.Disk

	if len(zones) == 0 {
		computeZones, err := c.GetComputeZones(ctx, project)
		if err != nil {
			return nil, err
		}
		for _, computeZone := range computeZones {
			zones = append(zones, computeZone.GetName())
		}
	}

	for _, zone := range zones {
		req := &computepb.ListDisksRequest{
			Project: project,
			Zone:    zone,
		}
		it := c.ComputeDisksClient.List(ctx, req)
		for {
			disk, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			disks = append(disks, disk)
		}
	}

	return disks, nil
}

// DeleteComputeDisks deletes the provided gcp compute disks
func (c *ClientStore) DeleteComputeDisks(ctx context.Context, disks []*computepb.Disk, dryRun bool) error {
	if dryRun {
		for _, disk := range disks {
			slog.InfoContext(ctx, fmt.Sprintf("gcp compute disk %s will be deleted", disk.GetSelfLink()), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}

// GetComputeNetworks returns a list of gcp compute networks in the provided gcp project and locations
func (c *ClientStore) GetComputeNetworks(ctx context.Context, project string, locations []string) ([]*computepb.Network, error) {
	var networks []*computepb.Network

	req := &computepb.ListNetworksRequest{
		Project: project,
	}
	it := c.ComputeNetworksClient.List(ctx, req)
	for {
		network, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		networks = append(networks, network)
	}

	return networks, nil
}

// DeleteComputeNetworks deletes the provided gcp compute networks
func (c *ClientStore) DeleteComputeNetworks(ctx context.Context, networks []*computepb.Network, dryRun bool) error {
	if dryRun {
		for _, network := range networks {
			slog.InfoContext(ctx, fmt.Sprintf("gcp compute network %s will be deleted", network.GetSelfLink()), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}

// GetComputeZones returns a list of gcp compute zones enabled for the provided gcp project
func (c *ClientStore) GetComputeZones(ctx context.Context, project string) ([]*computepb.Zone, error) {
	var zones []*computepb.Zone

	req := &computepb.ListZonesRequest{
		Project: project,
	}
	it := c.ComputeZonesClient.List(ctx, req)
	for {
		zone, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		zones = append(zones, zone)
	}

	return zones, nil
}

// GetComputeRegions returns a list of gcp compute regions enabled for the provided gcp project
func (c *ClientStore) GetComputeRegions(ctx context.Context, project string) ([]*computepb.Region, error) {
	var regions []*computepb.Region

	req := &computepb.ListRegionsRequest{
		Project: project,
	}
	it := c.ComputeRegionsClient.List(ctx, req)
	for {
		region, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		regions = append(regions, region)
	}

	return regions, nil
}
