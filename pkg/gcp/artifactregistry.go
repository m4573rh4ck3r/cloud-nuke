// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/artifactregistry/apiv1/artifactregistrypb"
	"google.golang.org/api/iterator"
)

// GetArtifactregistryRepositories returns a list of gcp artifactregistry repositories in the provided gcp project and locations
func (c *ClientStore) GetArtifactregistryRepositories(ctx context.Context, project string, locations []string) ([]*artifactregistrypb.Repository, error) {
	var repositories []*artifactregistrypb.Repository

	for _, location := range locations {
		req := &artifactregistrypb.ListRepositoriesRequest{
			Parent: fmt.Sprintf("projects/%s/locations/%s", project, location),
		}

		it := c.ArtifactregistryClient.ListRepositories(ctx, req)
		for {
			repository, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, err
			}
			repositories = append(repositories, repository)
		}
	}

	return repositories, nil
}

// DeleteArtifactregistryRepositories deletes the provided gcp artifactregistry repositories
func (c *ClientStore) DeleteArtifactregistryRepositories(ctx context.Context, artifactregistryRepositories []*artifactregistrypb.Repository, dryRun bool) error {
	if dryRun {
		for _, artifactregistryRepository := range artifactregistryRepositories {
			slog.InfoContext(ctx, fmt.Sprintf("artifactregistry repository %s will be deleted", artifactregistryRepository.GetName()), slog.Bool("dry-run", dryRun))
		}

		return nil
	}

	return nil
}
