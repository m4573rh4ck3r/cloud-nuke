// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"

	alloydb "cloud.google.com/go/alloydb/apiv1"
	"cloud.google.com/go/artifactregistry/apiv1"
	"cloud.google.com/go/bigquery"
	"cloud.google.com/go/compute/apiv1"
	container "cloud.google.com/go/container/apiv1"
	"cloud.google.com/go/dataproc/v2/apiv1"
	kms "cloud.google.com/go/kms/apiv1"
	redis "cloud.google.com/go/redis/apiv1"
	run "cloud.google.com/go/run/apiv2"
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	spanner "cloud.google.com/go/spanner/admin/instance/apiv1"
	storage "cloud.google.com/go/storage"
	tpu "cloud.google.com/go/tpu/apiv1"
	"google.golang.org/api/option"
)

type ClientStore struct {
	AlloyDBAdminClient           *alloydb.AlloyDBAdminClient
	ArtifactregistryClient       *artifactregistry.Client
	BigqueryClient               *bigquery.Client
	ComputeAddressesClient       *compute.AddressesClient
	ComputeBackendServicesClient *compute.BackendServicesClient
	ComputeDisksClient           *compute.DisksClient
	ComputeNetworksClient        *compute.NetworksClient
	ComputeRegionsClient         *compute.RegionsClient
	ComputeZonesClient           *compute.ZonesClient
	ContainerClustersClient      *container.ClusterManagerClient
	DataprocClustersClient       *dataproc.ClusterControllerClient
	KmsKeyManagementClient       *kms.KeyManagementClient
	RedisInstancesClient         *redis.CloudRedisClient
	RunServicesClient            *run.ServicesClient
	SecretmanagerClient          *secretmanager.Client
	SpannerClient                *spanner.InstanceAdminClient
	StorageBucketsClient         *storage.Client
	TpuClient                    *tpu.Client
}

func InitClients(ctx context.Context, opts *option.ClientOption, project string, apis []string) (*ClientStore, error) {
	clientStore := &ClientStore{}

	for _, api := range apis {
		switch api {
		case "alloydb":
			alloyDBAdminClient, err := alloydb.NewAlloyDBAdminClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize alloydb client: %v", err)
			}
			clientStore.AlloyDBAdminClient = alloyDBAdminClient
		case "artifactregistry":
			artifactregistryClient, err := artifactregistry.NewClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize artifactregistry client: %v", err)
			}
			clientStore.ArtifactregistryClient = artifactregistryClient
		case "bigquery":
			bigqueryClient, err := bigquery.NewClient(ctx, project, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize bigquery client: %v", err)
			}
			clientStore.BigqueryClient = bigqueryClient
		case "cloudkms":
			kmsKeyManagementClient, err := kms.NewKeyManagementClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize cloudkms client: %v", err)
			}
			clientStore.KmsKeyManagementClient = kmsKeyManagementClient
		case "compute":
			ComputeAddressesClient, err := compute.NewAddressesRESTClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize compute client: %v", err)
			}
			clientStore.ComputeAddressesClient = ComputeAddressesClient

			backendServicesClient, err := compute.NewBackendServicesRESTClient(ctx, *opts)
			if err != nil {
				return nil, err
			}
			clientStore.ComputeBackendServicesClient = backendServicesClient

			disksClient, err := compute.NewDisksRESTClient(ctx, *opts)
			if err != nil {
				return nil, err
			}
			clientStore.ComputeDisksClient = disksClient

			networksClient, err := compute.NewNetworksRESTClient(ctx, *opts)
			if err != nil {
				return nil, err
			}
			clientStore.ComputeNetworksClient = networksClient

			regionsClient, err := compute.NewRegionsRESTClient(ctx, *opts)
			if err != nil {
				return nil, err
			}
			clientStore.ComputeRegionsClient = regionsClient

			zonesClient, err := compute.NewZonesRESTClient(ctx, *opts)
			if err != nil {
				return nil, err
			}
			clientStore.ComputeZonesClient = zonesClient
		case "container":
			client, err := container.NewClusterManagerClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize container client: %v", err)
			}
			clientStore.ContainerClustersClient = client
		case "dataproc":
			dataprocClient, err := dataproc.NewClusterControllerClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize dataproc client: %v", err)
			}
			clientStore.DataprocClustersClient = dataprocClient
		case "redis":
			client, err := redis.NewCloudRedisClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize redis client: %v", err)
			}
			clientStore.RedisInstancesClient = client
		case "run":
			servicesClient, err := run.NewServicesClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize run client: %v", err)
			}
			clientStore.RunServicesClient = servicesClient
		case "secretmanager":
			secretmanagerClient, err := secretmanager.NewClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize secretmanager client: %v", err)
			}
			clientStore.SecretmanagerClient = secretmanagerClient
		case "spanner":
			spannerClient, err := spanner.NewInstanceAdminClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize spanner client: %v", err)
			}
			clientStore.SpannerClient = spannerClient
		case "storage-component":
			client, err := storage.NewClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize storage-component client: %v", err)
			}
			clientStore.StorageBucketsClient = client
		case "tpu":
			client, err := tpu.NewClient(ctx, *opts)
			if err != nil {
				return nil, fmt.Errorf("failed to initialize tpu client: %v", err)
			}
			clientStore.TpuClient = client
		default:
			return nil, fmt.Errorf("%s: unsupported api", api)
		}
	}

	return clientStore, nil
}
