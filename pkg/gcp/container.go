// Copyright 2024 M4573RH4CK3R
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gcp

import (
	"context"
	"fmt"
	"log/slog"

	"cloud.google.com/go/container/apiv1/containerpb"
)

// GetContainerClusters returns a list of gcp container clusters in the provided gcp project and locations
func (c *ClientStore) GetContainerClusters(ctx context.Context, project string, locations []string) ([]*containerpb.Cluster, error) {
	var clusters []*containerpb.Cluster

	for _, location := range locations {
		req := &containerpb.ListClustersRequest{
			Parent: fmt.Sprintf("projects/%s/locations/%s", project, location),
		}
		resp, err := c.ContainerClustersClient.ListClusters(ctx, req)
		if err != nil {
			return nil, err
		}
		clusters = append(clusters, resp.Clusters...)
	}

	return clusters, nil
}

// DeleteContainerClusters deletes the provided gcp container clusters
func (c *ClientStore) DeleteContainerClusters(ctx context.Context, containerClusters []*containerpb.Cluster, dryRun bool) error {
	if dryRun {
		for _, containerCluster := range containerClusters {
			slog.InfoContext(ctx, fmt.Sprintf("container cluster %s will be deleted", containerCluster.SelfLink), slog.Bool("dry-run", dryRun))
		}
		return nil
	}

	return nil
}
