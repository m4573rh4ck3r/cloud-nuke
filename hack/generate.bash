#!/bin/bash

set -eo pipefail

rm -fv cmd/cloud-nuke/cloud-nuke.go
go generate ./...
go fmt ./...
